(* * * * * * * * * * * * * * * === UMisc === * * * * * * * * * * * * * *
 *      Version: 1.1.0                                                 *
 * Release date: December 21st, 2014                                   *
 *      License: GNU GPL 3.0  (included with the source)               *
 *       Author: Igor Nunes, aka thoga31 @ www.portugal-a-programar.pt *
 *                                                                     *
 *  Description: Miscellaneous tools.                                  *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *)

{$mode objfpc}
unit UMisc;

interface
uses crt, sysutils, strutils, dateutils,
     UTypes;
     
type TKeys = set of char;

const CRLF = #13+#10;

function Max(x, y : word) : word;
function Min(x, y : word) : word;
function IsIntegral(s : string) : boolean;
function FormatSize(size : Int64) : string;
function OrdinalNumber(const n : word) : string;
function DateToMY(d : TDateTime) : string;
function FormatRange(range : TEditionRange) : string;
function FormatTimeToHNS(secs : Int64) : string;
function Int2Str(n : word; lim : word) : string;
function EdSetCount(bs : TEditionRange) : byte;

procedure Pause(const PauseText : string; const KeysToProceed : TKeys; var KeyReceiver : char);
procedure Pause(const PauseText : string; const KeysToProceed : TKeys); overload;
procedure Pause(const PauseText : string); overload;
procedure Pause; overload;



implementation

function Max(x, y : word) : word;
(* Maximum of two numbers. *)
begin
    if x > y then Max := x
             else Max := y;
end;


function Min(x, y : word) : word;
(* Minimum of two numbers. *)
begin
    if x < y then Min := x
             else Min := y;
end;


function IsIntegral(s : string) : boolean;
(* Is "s" an integral number? *)
var i, e : integer;
begin
    Val(s, i, e);
    IsIntegral := (e = 0);
end;


function FormatSize(size : Int64) : string;
(* Takes a size in bytes and converts it into a string with a proper scale. *)
const BYTESIZE : array[0..6] of string = ('B', 'KB', 'MB', 'GB', 'TB', 'PB', 'HB');
      CONV : double = 1024.0;
var finalsize : double;
    counter   : byte = 0;
begin
    FormatSize := '';
    finalsize := double(size);
    while finalsize > CONV do begin
        finalsize := finalsize / CONV;
        Inc(counter);
    end;
    FormatSize := FloatToStrF(finalsize, ffFixed, 15, 3) + ' ' + BYTESIZE[counter];
end;


function OrdinalNumber(const n : word) : string;
begin
    OrdinalNumber := IntToStr(n);
    if n in [10..20] then
        OrdinalNumber := OrdinalNumber + 'th'
    else
        case OrdinalNumber[Length(OrdinalNumber)] of
            '1' : OrdinalNumber := OrdinalNumber + 'st';
            '2' : OrdinalNumber := OrdinalNumber + 'nd';
            '3' : OrdinalNumber := OrdinalNumber + 'rd';
        else
            OrdinalNumber := OrdinalNumber + 'th';
        end;
end;


function DateToMY(d : TDateTime) : string;
begin
    DateToMY := LongMonthNames[MonthOf(d)] + ', ' + IntToStr(YearOf(d));
end;


function FormatRange(range : TEditionRange) : string;
var elem : word;
    last : word = 0;
    curr : word = 0;
    temp : string = '';
begin
    if range = [] then
        temp := 'NONE'
    else begin
        for elem in range do begin
            if last = 0 then begin
                last := elem;
                curr := elem;
                temp := IntToStr(last);
            end
            else if elem = curr+1 then begin
                curr := elem
            end else if elem > curr+1 then begin
                temp := temp + IfThen(last <> curr, IfThen(last = curr-1, ',', '-') + IntToStr(curr), '') + ',' + IntToStr(elem);
                last := elem;
                curr := elem;
            end;
        end;
        if curr <> last then
            temp := temp + IfThen(last = curr-1, ',', '-') + IntToStr(curr);
    end;
    FormatRange := temp;
end;


procedure Pause(const PauseText : string; const KeysToProceed : TKeys; var KeyReceiver : char);
(* FULL PROCEDURE - gets text to show, keys which unlock the pause state and the key receiver variable. *)
begin
     write(PauseText);
     repeat
           KeyReceiver := ReadKey;
     until KeyReceiver in KeysToProceed;
end;


procedure Pause(const PauseText : string; const KeysToProceed : TKeys); overload;
(* COMPACT PROCEDURE - without output (key receiver variable). *)
var Key : char;
begin
     Pause(PauseText, KeysToProceed, Key);
end;


procedure Pause(const PauseText : string); overload;
(* SIMPLIFIED PROCEDURE - it assumes Enter as the key to process. *)
begin
     Pause(PauseText, [#13]);
end;


procedure Pause; overload;
(* MINIMALIST PROCEDURE - it's the default behaviour: just waits for the Enter, without prompt nor key receiver. *)
const StrEmpty : string = '';
begin
     Pause(StrEmpty);
end;


function FormatTimeToHNS(secs : Int64) : string;
var h, n, s, ms : word;
begin
    ms := secs mod 1000;
    secs := secs div 1000;
    s := secs mod 60;
    secs := secs div 60;
    n := secs mod 60;
    secs := secs div 60;
    h := secs div 60;
    FormatTimeToHNS := IfThen(h > 0, Int2Str(h,2) + 'h', '') + IfThen(n > 0, Int2Str(n,2) + ':', '') + IfThen(s > 0, Int2Str(s,2) + '''', '') + Int2Str(ms,3) + '"';
end;


function Int2Str(n : word; lim : word) : string;
var i : word;
begin
    Int2Str := IntToStr(n);
    for i := lim - Length(Int2Str) downto 1 do
        Int2Str := '0' + Int2Str;
end;


function EdSetCount(bs : TEditionRange) : byte;
var e : byte;
begin
   EdSetCount := 0;
   for e in bs do
      Inc(EdSetCount);
end;

end.
