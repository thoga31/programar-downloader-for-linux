(* * * * * * * * * * * * * * === UParamValidation ===  * * * * * * * * *
 *      Version: 1.1.1                                                 *
 * Release date: December 21st, 2014                                   *
 *      License: GNU GPL 3.0  (included with the source)               *
 *       Author: Igor Nunes, aka thoga31 @ www.portugal-a-programar.pt *
 *                                                                     *
 *  Description: Validation of the parameters given from the shell.    *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *)

{$mode objfpc}
unit UParamValidation;

interface
uses sysutils, strutils, dateutils,
     UTypes, UMisc;

function ParamContains(p : string) : boolean;
function IsMode(p : string) : boolean;
function IsModifier(p : string) : boolean;
function ParamCountMode : byte;
function ParamCountModifier : byte;
function ParamHasMode : boolean;
function ParamHasModifier : boolean;
function PosOfParam(p : string) : word;

function GetAppMode : TAppMode;
function GetAppModifier : TAppModifier;

procedure ValidateArguments;



implementation

const MODIFIERID = '-';
      MODEID = '--';

function ParamContains(p : string) : boolean;
(* Checks if ParamStr() contains "p". *)
var i : word;
begin
    ParamContains := false;
    for i := 1 to ParamCount do
        if p = ParamStr(i) then begin
            ParamContains := true;
            break;
        end;
end;


function IsMode(p : string) : boolean;
(* Checks if "p" is a parameter which defines a mode. *)
begin
    IsMode := AnsiStartsText(MODEID, p);
end;


function IsModifier(p : string) : boolean;
(* Checks if "p" is a parameter which defines a modifier. *)
begin
    IsModifier := AnsiStartsText(MODIFIERID, p) and not IsMode(p);
end;


function ParamCountMode : byte;
(* How many parameters are modes? *)
var i : word;
begin
    ParamCountMode := 0;
    for i := 1 to ParamCount do
        if IsMode(ParamStr(i)) then
            Inc(ParamCountMode);
end;


function ParamCountModifier : byte;
(* How many parameters are modifiers? *)
var i : word;
begin
    ParamCountModifier := 0;
    for i := 1 to ParamCount do
        if IsModifier(ParamStr(i)) and not IsIntegral(ParamStr(i)) then
            Inc(ParamCountModifier);
end;


function ParamHasMode : boolean;
(* Is there any mode definition? *)
begin
    ParamHasMode := ParamCountMode > 0;
end;


function ParamHasModifier : boolean;
(* Is there any modifier definition? *)
begin
    ParamHasModifier := ParamCountModifier > 0;
end;


function PosOfParam(p : string) : word;
(* Where is paramter "p"? *)
var i : word;
begin
    PosOfParam := 0;
    for i := 1 to ParamCount do
        if p = ParamStr(i) then begin
            PosOfParam := i;
            break;
        end;
end;


function GetAppMode : TAppMode;
(* Analyses the parameters for mode definitions. *)
begin
    GetAppMode := ModeNull;
    
    if (ParamCountMode = 0) or ParamContains('--interact') then
        GetAppMode := ModeInteract;
    
    if ParamContains('--last') then
        GetAppMode := ModeLast;
    
    if ParamContains('--get') or ParamContains('--download') then
        GetAppMode := ModeGet;
    
    if ParamContains('--info') then
        GetAppMode := ModeInfo;
    
    if ParamContains('--help') then
        GetAppMode := ModeHelp;
    
    if ParamContains('--about') then
        GetAppMode := ModeAbout;
end;


function GetAppModifier : TAppModifier;
(* Analyses the parameters for modifiers. *)
begin
    GetAppModifier := ModDefault;
    
    if ParamContains('-v') then
        GetAppModifier := ModVerbose;
    
    if ParamContains('-q') then
        GetAppModifier := ModQuiet;
    
    if ParamContains('-o') then
        GetAppModifier := ModForceOffline;
end;


procedure ValidateArguments;
(* Performs an exhaustive validation of parameters. However, this doesn't handle everything. *)
var i : word;
begin
    // Basic validation: there is exactly 1 mode, as well 1 or 0 modifiers.
    
    if not ParamHasMode then begin
        if (ParamCount > 0) and not IsModifier(ParamStr(1)) then
            raise Exception.Create('Invalid parameters: too much parameters for --interact')
        else Exit;
    end;
    
    if ParamCountMode > 1 then
        raise Exception.Create('Invalid parameters: more than one mode detected.');
    
    if ParamCountModifier > 1 then
        raise Exception.Create('Invalid parameters: more than one modifier detected.');
    
    // Intermediate validation: if there are modifiers, is the order of mode and modifier proper?
    if ParamHasModifier then begin
        if not (IsModifier(ParamStr(1)) or IsModifier(ParamStr(ParamCount))) then
            raise Exception.Create('Invalid parameters: modifier is not the first nor the last parameter.');
        
        if IsModifier(ParamStr(1)) and (not IsMode(ParamStr(2))) then
            raise Exception.Create('Invalid parameters: mode doesn''t follow up after modifier.');
        
        if IsModifier(ParamStr(ParamCount)) and not IsMode(ParamStr(1)) then
            raise Exception.Create('Invalid parameters: mode is not the first parameter.');
    end else begin
        if not IsMode(ParamStr(1)) and ParamHasMode then
            raise Exception.Create('Invalid parameters: mode is not the first parameter.');
    end;
    
    // Special validation: validate intermediate parameters, specific for each mode.
    i := 1;
    while i <= ParamCount do begin
        if IsMode(ParamStr(i)) then begin
            case ParamStr(i) of
                '--help' : if (i < ParamCount) and not IsModifier(ParamStr(i+1)) then
                               raise Exception.Create('Invalid parameters: too much parameters for --help.');
                '--about' : if (i < ParamCount) and not IsModifier(ParamStr(i+1)) then
                               raise Exception.Create('Invalid parameters: too much parameters for --about.');
                '--info' :
                    begin
                        Inc(i);
                        if i > ParamCount then
                            raise Exception.Create('Invalid parameters: edition to inform about not defined.');
                        if not IsIntegral(ParamStr(i)) and not(ParamStr(i) = 'last') then
                            raise Exception.Create('Invalid parameters: not integral number.');
                        if not(StrToInt(ParamStr(i)) > 0) then
                            raise Exception.Create('Invalid parameters: only positive numbers are accepted.');
                    end;
                '--download', '--get' :
                    begin
                        Inc(i);
                        if i > ParamCount then
                            raise Exception.Create('Invalid parameters: download not defined.');
                        
                        case ParamStr(i) of
                            'all' : if (i < ParamCount) and not IsModifier(ParamStr(i+1)) then
                                        raise Exception.Create('Invalid parameters: too much parameters for --get all.');
                            'last' : if (i < ParamCount) and not IsModifier(ParamStr(i+1)) then
                                         raise Exception.Create('Invalid parameters: too much parameters for --get last.');
                            'range' :
                                begin
                                    if i+2 > ParamCount then
                                        raise Exception.Create('Invalid parameters: invalid range definition.');
                                    if (i+2 < ParamCount) and not IsModifier(ParamStr(i+3)) then
                                        raise Exception.Create('Invalid parameters: too much parameters for --get range.');
                                    if not(IsIntegral(ParamStr(i+1)) and IsIntegral(ParamStr(i+2))) then
                                        raise Exception.Create('Invalid parameters: not integral numbers for range.');
                                    if not((StrToInt(ParamStr(i+1)) > 0) and (StrToInt(ParamStr(i+2)) > 0)) then
                                        raise Exception.Create('Invalid parameters: range only accepts positive numbers.');
                                    Inc(i,3);
                                end;
                            'set' :
                                begin
                                    if i = ParamCount then
                                        raise Exception.Create('Invalid parameters: no set definition.');
                                    Inc(i);
                                    while i <= ParamCount do begin
                                        if not IsIntegral(ParamStr(i)) then begin
                                            if not IsModifier(ParamStr(i)) then
                                                raise Exception.Create('Invalid parameters: invalid set definition, no integral number.');
                                            break;
                                        end;
                                        Inc(i);
                                    end;
                                end;
                        else
                            raise Exception.Create('Invalid parameters: download unknown definition.');
                        end;
                    end;
                '--last' : if (i < ParamCount) and not IsModifier(ParamStr(i+1)) then
                               raise Exception.Create('Invalid parameters: too much parameters for --last');
                '--interact' : if (i < ParamCount) and not IsModifier(ParamStr(i+1)) then
                                   raise Exception.Create('Invalid parameters: too much parameters for --interact');
            else
                raise Exception.Create('Invalid parameters: unknow mode.');
            end;
        end;
        
        Inc(i);
    end;
end;


end.
