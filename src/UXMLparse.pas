(* * * * * * * * * * * * * * === UXMLParse === * * * * * * * * * * * * * 
 *      Version: 1.1.2                                                 *
 * Release date: December 21st, 2014                                   *
 *      License: GNU GPL 3.0  (included with the source)               *
 *       Author: Igor Nunes, aka thoga31 @ www.portugal-a-programar.pt *
 *                                                                     *
 *  Description: Gets the XML, interpretates and integrates its info.  *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *)

{$mode objfpc}
unit UXMLparse;

interface
uses sysutils, strutils, classes, dateutils,
     httpsend, synacode,
     UTypes;

const PROGRAMAR_INFO_XML = 'http://programar.sergioribeiro.com/programar.xml';
      PROGRAMAR_VERSION_XML = 'http://programar.sergioribeiro.com/ver.xml';
      MAGAZINE_DB = 'eds.programar';
      CRLF = #13+#10;

type TRevistaPROGRAMAR = class(TObject)
         private
             var vultima : TEdicaoInfo;
                 vedicoes : TEdicoes;
                 vgotcollection : boolean;
             
             function GetMagazineCollection(var offline : boolean) : boolean;
             
         public
             property ultima  : TEdicaoInfo   read vultima;
             property edicoes : TEdicoes      read vedicoes;
             property HasCollection : boolean read vgotcollection;
             
             function GotCollection(var offline : boolean) : boolean;
             
             constructor Create(contact_server : boolean = false);
     end;


function IsTemaCapa(art : TEdicaoArtigo) : boolean;
function GetTemaCapa(ed : word) : TEdicaoArtigo;



implementation
uses UProgDown;

const TAG_edicoes         = '<edicoes>';
      TAG_EDICAO          = '<edicao ';
      ID_EDICAO_NUM       = 'num=';
      ID_EDICAO_DATA      = 'data=';
      
      TAG_IMAGENS              = '<imagens>';
      TAG_IMAGENS_CAPAS        = '<capas>';
      TAG_IMAGENS_CAPA_END     = '</capa>';
      TAG_IMAGENS_CAPA_PEQUENA = '<capa tamanho="pequena">';
      TAG_IMAGENS_CAPA_MEDIA   = '<capa tamanho="media">';
      TAG_IMAGENS_CAPA_GRANDE  = '<capa tamanho="grande">';
      
      TAG_PAGINAS             = '<paginas>';
      TAG_PAGINA              = '<pagina>';
      TAG_ARTIGOS             = '<artigos>';
      TAG_CATEGORIA           = '<categoria ';
      TAG_ARTIGO              = '<artigo ';
      TAG_ARTIGO_END          = '</artigo>';
      ID_ARTIGO_TITULO        = 'titulo=';
      ID_CATEGORIA_DESIGNACAO = 'designacao=';
      
      TAG_EQUIPA        = '<equipa>';
      TAG_EQUIPA_MEMBRO = '<membro>';
      
      TAG_PDF = '<pdf>';
      
      TAG_vultimaEDICAO_DATA   = '<ultimaedicao>';
      TAG_vultimaEDICAO_NUMERO = '<totaledicoes>';
      
      ID_MARKER = '"';


function IsTemaCapa(art : TEdicaoArtigo) : boolean;
begin
    IsTemaCapa := UpCase(art.categoria) = 'TEMA DE CAPA';
end;

function GetTemaCapa(ed : word) : TEdicaoArtigo;
var art : TEdicaoArtigo;
begin
    for art in PROGRAMAR.edicoes[ed-1].artigos do
        if IsTemaCapa(art) then begin
            GetTemaCapa := art;
            break;
        end;
            
end;

function NegateXMLTag(tag : string) : string;
begin
    if tag[2] = '/' then
        Delete(tag, 2, 1)
    else
        Insert('/', tag, 2);
    NegateXMLTag := tag;
end;


function GetXML(const XMLURL : string; var page : TStringList) : boolean;
begin
    page.Clear;
    GetXML := HTTPGetText(XMLURL, page);
end;


constructor TRevistaPROGRAMAR.Create(contact_server : boolean = false);
var foo : boolean;
begin
    self.vgotcollection := contact_server;
    if contact_server then
        self.vgotcollection := self.GetMagazineCollection(foo);
end;


function TRevistaPROGRAMAR.GotCollection(var offline : boolean) : boolean;
begin
    if not self.vgotcollection then
        self.vgotcollection := self.GetMagazineCollection(offline);
    GotCollection := self.vgotcollection;
end;


function TRevistaPROGRAMAR.GetMagazineCollection(var offline : boolean) : boolean;
var edition  : word = 0;
    line     : string;
    i, j     : word;
    temp     : string;
    category : string = '';
    page : TStringList;

begin
    page := TStringList.Create;
    GetMagazineCollection := false;
    try
        try
            if not offline then begin
               if not GetXML(PROGRAMAR_INFO_XML, page) then
                  offline := true;
            end;
            if offline then
               page.LoadFromFile(MAGAZINE_DB);
            
            for j := 0 to page.Count - 1 do begin
                line := TrimLeft(page[j]);
                
                // last edition
                if AnsiStartsText(TAG_vultimaEDICAO_NUMERO, line) then
                    self.vultima.numero := StrToInt( AnsiReplaceText(AnsiReplaceText(line, TAG_vultimaEDICAO_NUMERO, ''), NegateXMLTag(TAG_vultimaEDICAO_NUMERO), '') )
                else if AnsiStartsText(TAG_vultimaEDICAO_DATA, line) then begin
                    i := Pos(TAG_vultimaEDICAO_DATA, line) + Length(TAG_vultimaEDICAO_DATA);
                    temp := '';
                    while (line[i] <> '<') and (i <= Length(line)) do begin
                        temp := temp + line[i];
                        Inc(i);
                    end;
                    self.vultima.data := EncodeDateTime(StrToInt(Copy(temp, 4, 4)), StrToInt(Copy(temp, 1, 2)), 1, 0, 0, 0, 0);
                end
                
                // information about this edition
                else if AnsiStartsText(TAG_EDICAO, line) then begin
                    i := Pos(ID_EDICAO_NUM, line) + Length(ID_EDICAO_NUM) + 1;
                    temp := '';
                    while (line[i] <> '"') and (i <= Length(line)) do begin
                        temp := temp + line[i];
                        Inc(i);
                    end;
                    
                    edition := StrToInt(temp) - 1;
                    SetLength(self.vedicoes, edition + 1);
                    self.vedicoes[edition].info.numero := edition + 1;
                    
                    i := Pos(ID_EDICAO_DATA, line) + Length(ID_EDICAO_DATA) + 1;
                    temp := '';
                    while (line[i] <> '"') and (i <= Length(line)) do begin
                        temp := temp + line[i];
                        Inc(i);
                    end;
                    self.vedicoes[edition].info.data := EncodeDateTime(StrToInt(Copy(temp, 4, 4)), StrToInt(Copy(temp, 1, 2)), 1, 0, 0, 0, 0);
                end
                
                // frontpage images
                else if AnsiStartsText(TAG_IMAGENS_CAPA_PEQUENA, line) then
                    self.vedicoes[edition].imagens.capa.pequena := AnsiReplaceText(AnsiReplaceText(line, TAG_IMAGENS_CAPA_PEQUENA, ''), TAG_IMAGENS_CAPA_END, '')
                else if AnsiStartsText(TAG_IMAGENS_CAPA_MEDIA, line) then
                    self.vedicoes[edition].imagens.capa.media := AnsiReplaceText(AnsiReplaceText(line, TAG_IMAGENS_CAPA_MEDIA, ''), TAG_IMAGENS_CAPA_END, '')
                else if AnsiStartsText(TAG_IMAGENS_CAPA_GRANDE, line) then
                    self.vedicoes[edition].imagens.capa.grande := AnsiReplaceText(AnsiReplaceText(line, TAG_IMAGENS_CAPA_GRANDE, ''), TAG_IMAGENS_CAPA_END, '')
                
                // pages images
                else if AnsiStartsText(TAG_PAGINA, line) then begin
                    SetLength(self.vedicoes[edition].imagens.paginas, Length(self.vedicoes[edition].imagens.paginas) + 1);
                    self.vedicoes[edition].imagens.paginas[High(self.vedicoes[edition].imagens.paginas)] := AnsiReplaceText(AnsiReplaceText(line, TAG_PAGINA, ''), NegateXMLTag(TAG_PAGINA), '');
                end
                
                // new category
                else if AnsiStartsText(TAG_CATEGORIA, line) then begin
                    i := Pos(ID_CATEGORIA_DESIGNACAO, line) + Length(ID_CATEGORIA_DESIGNACAO) + 1;
                    temp := '';
                    while (line[i] <> '"') and (i <= Length(line)) do begin
                        temp := temp + line[i];
                        Inc(i);
                    end;
                    category := temp;
                end
                
                // articles
                else if AnsiStartsText(TAG_ARTIGO, line) then begin
                    i := Pos(ID_ARTIGO_TITULO, line) + Length(ID_ARTIGO_TITULO) + 1;
                    temp := '';
                    while (line[i] <> '"') and (i <= Length(line)) do begin
                        temp := temp + line[i];
                        Inc(i);
                    end;
                    Inc(i);
                    SetLength(self.vedicoes[edition].artigos, Length(self.vedicoes[edition].artigos) + 1);
                    self.vedicoes[edition].artigos[High(self.vedicoes[edition].artigos)].titulo := temp;
                    self.vedicoes[edition].artigos[High(self.vedicoes[edition].artigos)].categoria := category;
                    
                    temp := line;
                    Delete(temp, 1, i);
                    self.vedicoes[edition].artigos[High(self.vedicoes[edition].artigos)].descricao := AnsiReplaceText(temp, TAG_ARTIGO_END, '');
                end
                
                //team
                else if AnsiStartsText(TAG_EQUIPA_MEMBRO, line) then begin
                    temp := AnsiReplaceText(line, TAG_EQUIPA_MEMBRO, '');
                    temp := AnsiReplaceText(temp, NegateXMLTag(TAG_EQUIPA_MEMBRO), '');
                    SetLength(self.vedicoes[edition].equipa, Length(self.vedicoes[edition].equipa) + 1);
                    self.vedicoes[edition].equipa[High(self.vedicoes[edition].equipa)] := temp;
                end
                
                // pdf
                else if AnsiStartsText(TAG_PDF, line) then
                    self.vedicoes[edition].pdf := AnsiReplaceText(AnsiReplaceText(line, TAG_PDF, ''), NegateXMLTag(TAG_PDF), '');
            end;
            
            GetMagazineCollection := true;
            if not offline then
               page.SaveToFile(MAGAZINE_DB);
            
        except
            on ex : exception do begin
                writeln;
                writeln('ERRO: ', ex.classname, ' -> ', ex.message);
                writeln;
            end;
        end;
    finally
        page.Destroy;
    end;
end;

end.
