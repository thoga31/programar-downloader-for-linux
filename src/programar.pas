(* * * * * * * * * * * === PROGRAMAR  Downloader === * * * * * * * * * * 
 *      Version: 2.1.1                                                 *
 * Release date: December 21st, 2014                                   *
 *      License: GNU GPL 3.0  (included with the source)               *
 *       Author: Igor Nunes, aka thoga31 @ www.portugal-a-programar.pt *
 *                                                                     *
 *  Description: The main program.                                     *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *)

{$mode objfpc}
program revista;

uses {$ifdef unix} cthreads, cmem, {$endif}
     crt, strutils, sysutils, classes,  // RTL
     httpsend, synacode,                // Synapse
     UTypes, UProgDown, UMisc, UParamValidation, UGUIintegrator;

var lastedition : TEdicaoInfo;
    currentpath : string;
    modifier : TAppModifier = ModDefault;


procedure ShowHelp;
(* The help topic. *)
begin
    textcolor(10);
    writeln('Usage: revista [--interact | --last | <--get|--download> <all|last|range|set>] [modifier]');
    textcolor(15);
    writeln('Parameter    Description');
    textcolor(11);
    writeln('--interact   Starts the interactive mode. The same as no mode defined.');
    writeln('--last       Gets the number of last edition.');
    writeln('--get or --download');
    writeln('             Downloads the given editions.');
    textcolor(7);
    writeln('   last      Gets the last edition.');
    writeln('   all       Gets all the editions.');
    writeln('   range min max');
    writeln('             Gets editions from ''min'' to ''max''.');
    writeln('   set [n]   Gets the given list of editions (separated by space, not comma).');
    textcolor(11);
    writeln('--info n     Gets information about nth edition. If n=''last'', shows last ed.');
    writeln('--about      Information abou this application.');
    writeln('--help       Shows this topic.');
    textcolor(15);
    writeln('Modifier    Description');
    textcolor(7);
    writeln('   -v       Verbose: shows every information.');
    writeln('   -q       Quite: all informers are turned off. No information is displayed.');
    writeln('   -o       Force offline: loads information from the local file.');
    write('** Press ENTER **');
    Pause;
    GotoXY(1, WhereY);
    ClrEol;
    textcolor(10);
    writeln('Examples:');
    textcolor(7);
    writeln('revista --last');
    writeln('revista --download last -q');
    writeln('revista --get range 23 29 -v');
    writeln('revista --get set 10 15 19 20 21 26');
    writeln('revista --info last -o');
end;


procedure Perform(mode : TAppMode; offline : boolean = false);
(* Performs the necessary actions for the given mode. *)
var i : word;
    position : word = 0;
    (* Editions to be downloaded *)
    eds : TEditionRange = [];

begin
    case mode of
        ModeInteract : InteractiveMode(PROGRAMAR, currentpath, offline);
        
        ModeLast :
            begin
                write('The last edition is the ');
                textcolor(10);
                writeln(OrdinalNumber(lastedition.numero), ' of ', DateToMY(lastedition.data), '.');
                textcolor(7);
            end;
        
        ModeGet :
            begin
            if not offline then begin
                position := PosOfParam('--get');
                if position = 0 then
                    position := PosOfParam('--download');
                Inc(position);
                
                case ParamStr(position) of
                    'all'   : eds := [1..lastedition.numero];
                    'last'  : eds := [lastedition.numero];
                    'range' : eds := [Min(StrToInt(ParamStr(position+1)), StrToInt(ParamStr(position+2))) .. Max(StrToInt(ParamStr(position+1)), StrToInt(ParamStr(position+2)))];
                    'set'   :
                        begin
                            for i := position+1 to ParamCount do begin
                                if (not IsIntegral(ParamStr(i))) or IsModifier(ParamStr(i)) then
                                    break;
                                Include(eds, StrToInt(ParamStr(i)));
                            end;
                        end;
                end;
                
                // Report which editions will be downloaded:
                if modifier <> ModQuiet then
                    writeln('Editions to download: ', FormatRange(eds));
                
                if eds <> [] then
                    DownloadMagazine(eds, currentpath, modifier)
                else  // This shouldn't happen. However, for precaution, we'll have this alert.
                    if modifier <> ModQuiet then
                        writeln('  * There are no editions to download!');
            end else begin
                writeln('INFO: Cannot download in offline mode.');
                writeln('      Process aborted.');
            end;
            end;
            
        ModeInfo :
            begin
                position := PosOfParam('--info');
                Inc(position);
                i := StrToInt(IfThen(ParamStr(position) = 'last', IntToStr(lastedition.numero), ParamStr(position)));
                if i <= lastedition.numero then begin
                    ShowInfoOf(PROGRAMAR, i, mode, offline);
                    GotoXY(1, SCREENHEIGHT-1);
                end else
                    writeln('ERROR! You asked for the ', OrdinalNumber(i),' edition, but there are only ', lastedition.numero, ' editions!');
            end;
    end;
end;


(* MAIN BLOCK *)
var mode : TAppMode = ModeNull;
    ico : TLoadIcon;
    offline : boolean = false;

begin
    try
       ico := TLoadIcon.Create(false);
       try
           // clrscr;
           ShowTitle;
           ValidateArguments; // Validate arguments
           
           // Gets mode and modifier
           mode := GetAppMode;
           
           // If it's Help Mode, we need nothing else...
           if mode = ModeHelp then
               ShowHelp
           else if mode = ModeAbout then
               ShowAbout
           else begin
               
               modifier := GetAppModifier;
               
               if modifier = ModForceOffline then
                  offline := true;
               
               // Sets the path:
               currentpath := GetCurrentDir + '/';
               
               // Mode and modifier notification
               if modifier = ModVerbose then begin
                   writeln('    Mode = ', mode);
                   writeln('Modifier = ', modifier);
               end else if modifier = ModQuiet then begin
                   writeln('Modifier = ', modifier);
                   if mode = ModeGet then
                       writeln('   * ALERT! You won''t be informed if any error during download occurrs.');
               end;
               
               // Current directory notification
               if (mode <> ModeHelp) and (modifier <> ModQuiet) and (modifier <> ModForceOffline) then begin
                   writeln('Current directory = "', GetCurrentDir, '"');
                   write('Connecting to the server... ');
                   ico.SetPosition(WhereX, WhereY);
                   ico.Run := true;
               end;
               
               if not PROGRAMAR.GotCollection(offline) then begin
                   if (mode <> ModeHelp) and (modifier <> ModQuiet) and (modifier <> ModForceOffline) then
                      ico.Run := false;
                   writeln(CRLF, 'FATAL: program cannot proceed');
                   raise Exception.Create('Cannot connect to the server nor load the offline file.');
               end else
                  if (mode <> ModeHelp) and (modifier <> ModQuiet) and (modifier <> ModForceOffline) then
                     ico.Run := false;
               
               // Gets last edition from official site
               lastedition := PROGRAMAR.ultima;
               if lastedition.numero <= 0 then begin
                   writeln(CRLF, 'FATAL: program cannot proceed');
                   raise Exception.Create('Cannot get information about last edition.');
               end;
               if (modifier <> ModQuiet) then
                  case offline of
                     false : writeln('[OK]');
                     true  : if (modifier = ModForceOffline) then
                                writeln('NOTE: forced offline mode.');
                             else
                                writeln(CRLF, '   * Cannot connect! Working in offline mode.');
                  end;
               // Everything is checked and OK: perform actions.
               Perform(mode, offline);
               
               // Report end of program
               if (modifier <> ModQuiet) and (mode <> ModeHelp) then
                   writeln('Done.');
               
           end;
               
       except
           on ex:Exception do
               ShowException(ex);  // Unexpected or internal error
       end
    finally
       ico.Destroy;
    end;
end.
