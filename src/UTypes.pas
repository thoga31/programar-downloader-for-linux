(* * * * * * * * * * * * * * === UTypes ===  * * * * * * * * * * * * * *
 *      Version: 1.0.2                                                 *
 * Release date: December 21st, 2014                                   *
 *      License: GNU GPL 3.0  (included with the source)               *
 *       Author: Igor Nunes, aka thoga31 @ www.portugal-a-programar.pt *
 *                                                                     *
 *  Description: Compiles the main types used in the program.          *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *)

{$mode objfpc}
unit UTypes;

interface
uses classes;

type TEdicaoImagens = record
         capa : record
             pequena, media, grande : string;
         end;
         paginas : array of string;
     end;
     
     TEdicaoArtigo = record
         categoria : string;
         titulo    : string;
         descricao : string;
     end;
     
     TEdicaoInfo = record
         numero : word;
         data   : TDateTime;
     end;
     
     TEdicaoMembro       = string;
     TEdicaoDownloadLink = string;
     
     TEdicao = record
         info    : TEdicaoInfo;
         imagens : TEdicaoImagens;
         artigos : array of TEdicaoArtigo;
         equipa  : array of TEdicaoMembro;
         pdf     : TEdicaoDownloadLink;
     end;
     
     TEdicoes = array of TEdicao;
     TEdDB = Text;
     
     TEditionRange = set of byte;
     TAppMode      = (ModeNull, ModeLast, ModeGet, ModeInfo, ModeInteract, ModeHelp, ModeAbout);
     TAppModifier  = (ModDefault, ModVerbose, ModQuiet, ModForceOffline);
     
     TPoint = packed record
                 x, y : word;
              end;


implementation
    // void

end.
