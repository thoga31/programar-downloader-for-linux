(* * * * * * * * * * * * * * === UGUIintegrator ===  * * * * * * * * * *
 *      Version: 1.1.1                                                 *
 * Release date: December 21st, 2014                                   *
 *      License: GNU GPL 3.0  (included with the source)               *
 *       Author: Igor Nunes, aka thoga31 @ www.portugal-a-programar.pt *
 *                                                                     *
 *  Description: Some tools to integrate the GUI - work in progress.   *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *)

{$mode objfpc}
unit UGUIintegrator;

interface
uses {$ifdef unix} cthreads, cmem, {$endif}
     crt, sysutils, strutils, classes,
     UTypes, UXMLparse, UMisc;

const (* Application related information *)
      APPNAME           = 'PROGRAMAR Downloader';
      APPVERSION        = '2.1.1';
      APPDATE           = 'December 21st, 2014';
      APPBUILD          = '211214211026'; //ddmmyyvvvbbb (v=version, b=build)
      APPAUTHOR         = 'Igor Nunes';
      APPAUTHORNICK     = 'thoga31';
      APPAUTHORNICKSITE = 'http://www.portugal-a-programar.pt';
      (* Official site related information *)
      PAGE_DOWNLOAD = 'http://www.portugal-a-programar.pt/revista-programar/edicoes/download.php?e=';


type TLoadIcon = class(TThread)
        (* Produces an animated loading icon for processes that take a long time. *)
        private
           const INDICATORS = '-\|/';
           var vInd : byte;
               vPos : TPoint;
               vRun : boolean;
           
           procedure SetSuspension(myRun : boolean);
           
        protected
           procedure Execute; override;
           
        public
           constructor Create(CreateSuspended : boolean);
           procedure SetPosition(const myX, myY : word);
           property Position : TPoint read vPos;
           property Run : boolean write SetSuspension;
     end;


procedure ShowTitle;
procedure ShowAbout;
procedure ShowException(ex : Exception);

procedure ShowInfoOf(const PROGRAMAR : TRevistaProgramar; edition : word; const mode : TAppMode = ModeInteract; offline : boolean = false);
procedure InteractiveMode(const PROGRAMAR : TRevistaProgramar; const currentpath : string; offline : boolean = false);



implementation
uses UProgDown;

constructor TLoadIcon.Create(CreateSuspended : boolean);
begin
   FreeOnTerminate := false;
   vInd := 1;
   inherited Create(CreateSuspended);
end;


procedure TLoadIcon.SetPosition(const myX, myY : word);
begin
   self.vPos.x := myX;
   self.vPos.y := myY;
end;


procedure TLoadIcon.SetSuspension(myRun : boolean);
begin
   self.vRun := myRun;
end;


procedure TLoadIcon.Execute;
begin
   while (not terminated) do begin
      if self.vRun then begin
         GotoXY(self.vPos.x, self.vPos.y);
         write(self.INDICATORS[self.vInd], #8);
         Inc(self.vInd);
         if self.vInd > 4 then
            self.vInd := 1;
      end;
      Sleep(100);
   end;
end;


procedure ShowTitle;
(* Title of the application, to be shown at initialization. *)
begin
    textcolor(15);
    writeln(APPNAME, ' ', APPVERSION);
    textcolor(7);
    writeln('By ', APPAUTHOR, ' aka ', APPAUTHORNICK, ' @ ', APPAUTHORNICKSITE);
end;


procedure ShowAbout;
begin
    ClrScr;
    textcolor(15);
    writeln(PadCenter(APPNAME, 79));
    writeln(PadCenter(APPVERSION, 79));
    textcolor(7);
    writeln;
    writeln('Build ', APPBUILD, ', of ', APPDATE);
    writeln('By ', APPAUTHOR, ', aka ', APPAUTHORNICK, ' @ ', APPAUTHORNICKSITE);
    writeln;
    writeln('See the documentation provided for more information about this application.');
end;


procedure ShowException(ex : Exception);
(* Takes an exception and displays the information about it. *)
begin
    textbackground(0);
    textcolor(12);
    writeln(CRLF, '(Exception ', ex.classname, ') => ', ex.message);
    textcolor(7);
end;



(* INTERACTIVE MODE *)
var seted : TEditionRange = [];
    TOP : byte = 5;  // acts like a constant, except if mode <> ModeInteract

    (* Generic Tools *)
    procedure DrawHeader(const mode : TAppMode = ModeInteract; offline : boolean = false);
    // Private
    const TITLE_BASE = APPNAME + ' ' + APPVERSION;
          INDICATORS_OFFLINE = 'ENTER=go to, ESC=exit';
          INDICATORS_ONLINE = 'S=un/select, U=unselect all, G=get selection, D=download, ' + INDICATORS_OFFLINE;
    var i : word;
        INDICATORS : string = '';
        TITLE : string = '';
    begin
        if offline then
           TITLE := TITLE_BASE + ' (offline mode)'
        else
           TITLE := TITLE_BASE;
        TextBackground(7);
        TextColor(0);
        for i:=1 to SCREENWIDTH do
            write(' ');
        GotoXY(SCREENWIDTH div 2 - Length(TITLE) div 2, 1);
        write(TITLE);
        TextBackground(0);
        
        if mode = ModeInteract then begin
           if offline then
              INDICATORS := INDICATORS_OFFLINE
           else
              INDICATORS := INDICATORS_ONLINE;
           TextColor(8);
           GotoXY(SCREENWIDTH - Length(INDICATORS), 2);
           write(INDICATORS);
           textcolor(7);
           if not offline then begin
              GotoXY(1, 3);
              write('Current selection: ', FormatRange(seted));
           end;
        end else
           TextColor(7);
    end;

    procedure ResetWindow(const mode : TAppMode = ModeInteract; offline : boolean = false);
    // Private
    begin
        TextBackground(0);
        TextColor(7);
        ClrScr;
        DrawHeader(mode, offline);
    end;
    
    procedure ShowInfoOf(const PROGRAMAR : TRevistaProgramar; edition : word; offline : boolean = false); overload;
    begin
        ShowInfoOf(PROGRAMAR, edition, ModeInteract, offline);
    end;
    
    procedure ShowInfoOf(const PROGRAMAR : TRevistaProgramar; edition : word; const mode : TAppMode = ModeInteract; offline : boolean = false);
    // Public
    var elem   : string;
        art    : TEdicaoArtigo;
        height : word;
    begin
        ResetWindow(mode, offline);
        if mode <> ModeInteract then
            TOP := 3;
        GotoXY(3, TOP);
        if (mode = ModeInteract) and not offline then
            if edition in seted then begin
                textcolor(12);
                write('[X] ');
            end else begin
                textcolor(04);
                write('[ ] ');
            end;
        
        Dec(edition);
        TextColor(10); write(OrdinalNumber(PROGRAMAR.edicoes[edition].info.numero), ' edition of ', DateToMY(PROGRAMAR.edicoes[edition].info.data));
        if PROGRAMAR.edicoes[edition].info.numero = PROGRAMAR.ultima.numero then begin
            TextColor(14);
            write(' (last edition)');
        end;
        
        TextColor(15);
        GotoXY(3, TOP+1); write('Cover article: ');
        TextColor(7);
        for art in PROGRAMAR.edicoes[edition].artigos do
            if IsTemaCapa(art) then begin
                write(Copy(art.titulo, 1, StrToInt(IfThen(Length(art.titulo)>59, '56', '59'))), IfThen(Length(art.titulo)>59, '...', ''));
                break;
            end;
        
        GotoXY(3, TOP+3); TextColor(15); writeln('Team:'); TextColor(7);
        height := TOP+4;
        for elem in PROGRAMAR.edicoes[edition].equipa do begin
            if height >= SCREENHEIGHT-1 then begin
                write('  (...)');
                break;
            end;
            writeln('  ', Copy(elem, 1, StrToInt(IfThen(Length(elem)>20, '17', '20'))), IfThen(Length(elem)>20, '...', ''));
            Inc(height);
        end;
        
        GotoXY(25, TOP+3); TextColor(15); write('Articles:'); TextColor(7);
        height := TOP+4;
        for art in PROGRAMAR.edicoes[edition].artigos do begin
            GotoXY(25, WhereY+1);
            if height >= SCREENHEIGHT-1 then begin
                write('(...)');
                break;
            end;
            if not IsTemaCapa(art) then begin
                write(Copy(art.titulo, 1, StrToInt(IfThen(Length(art.titulo)>53, '50', '53'))), IfThen(Length(art.titulo)>53, '...', ''));
                Inc(height);
            end;
        end;
    end;


procedure InteractiveMode(const PROGRAMAR : TRevistaProgramar; const currentpath : string; offline : boolean = false);
var lastedition : TEdicaoInfo;

    procedure IncEd(var ed : word);
    begin
        Inc(ed);
        if ed > PROGRAMAR.ultima.numero then
            ed := 1;
    end;
    
    procedure DecEd(var ed : word);
    begin
        Dec(ed);
        if ed < 1 then
            ed := PROGRAMAR.ultima.numero;
    end;
    
    function GetNewEdition(ed : word) : word;
    var newed : word = 0;
        temp : string = '0';
    begin
        repeat
            GotoXY(1, SCREENHEIGHT-1);
            TextBackground(7);
            TextColor(0);
            ClrEol;
            
            if (newed > lastedition.numero) then begin
                GotoXY(54, SCREENHEIGHT-1);
                write('Greater than last edition!');
                newed := 0;
            end;
            
            if not IsIntegral(temp) then begin
                GotoXY(65, SCREENHEIGHT-1);
                write('Invalid input!');
            end;
            
            GotoXY(1, SCREENHEIGHT-1);
            write(' Go to (c = cancel, l = last [', OrdinalNumber(lastedition.numero), ']): ');
            readln(temp);
            
            if IsIntegral(temp) then
                newed := StrToInt(temp)
            else if UpCase(temp) = 'C' then
                break
            else if UpCase(temp) = 'L' then
                newed := lastedition.numero;
        until newed in [1..lastedition.numero];
        
        if newed = 0 then
            GetNewEdition := ed
        else
            GetNewEdition := newed;

        TextBackground(0);
        TextColor(7);
    end;

    procedure DownloadSet(eds : TEditionRange);
    var e : word;
        totalsize : Int64 = 0;
        time : Int64;
        totaltime : Int64 = 0;
        totaleds : byte;
        doneeds : byte = 0;
        abruptend : boolean = false;
        
        procedure ResetMiniWindow;
        var y : word;
        begin
            for y:=SCREENHEIGHT-5 to SCREENHEIGHT-1 do begin
                GotoXY(1, y);
                ClrEol;
            end;
            GotoXY(1, SCREENHEIGHT-5);
            write('Editions to download: ', FormatRange(eds));
        end;
        
    begin
        TextBackGround(7);
        TextColor(0);
        totaleds := EdSetCount(eds);
        ResetMiniWindow;
        for e in eds do begin
            GotoXY(1, SCREENHEIGHT-2);
            write(doneeds/totaleds*100:5:1, '% done (', doneeds, ' out of ', totaleds, ')');
            GotoXY(1, SCREENHEIGHT-4);
            Inc(totalsize, DownloadMagazineUI([e], currentpath, time, false, abruptend));
            Inc(totaltime, time);
            Inc(doneeds);
            ResetMiniWindow;
            if abruptend then
               break;
        end;
        if eds = [] then
            writeln(CRLF, '  * There are no editions to download!')
        else
            writeln(CRLF, CRLF, 'Total downloaded = ', FormatSize(totalsize), ' in ', FormatTimeToHNS(totaltime));
        write('Press ENTER to go back...');
        readln;
    end;


var key : char;
    ed  : word;

begin
    lastedition := PROGRAMAR.ultima;
    ed := lastedition.numero;
    ShowInfoOf(PROGRAMAR, ed, offline);
    repeat
        key := UpCase(readkey);  // NUL + [75 left, 77 right]
        if key = #0 then begin
            key := UpCase(readkey);
            if key in [#75, #77] then begin
                case key of
                    #75 : DecEd(ed);
                    #77 : IncEd(ed);
                end;
                ShowInfoOf(PROGRAMAR, ed, offline);
            end;
        end
        else begin
            if key = #13 then
               ed := GetNewEdition(ed)
            else if not offline then begin
               case key of
                  #83 : if ed in seted then
                            Exclude(seted, ed)
                        else
                            Include(seted, ed);
                  #85 : seted := [];
                  #71 : DownloadSet(seted);
                  #68 : DownloadSet([ed]);
               end;
            end;
            
            ShowInfoOf(PROGRAMAR, ed, offline);
        end;
    until key in [#27];
    ClrScr;
end;

end.
