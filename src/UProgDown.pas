(* * * * * * * * * * * * * * === UProgDown === * * * * * * * * * * * * * 
 *      Version: 1.4.1                                                 *
 * Release date: December 21st, 2014                                   *
 *      License: GNU GPL 3.0  (included with the source)               *
 *       Author: Igor Nunes, aka thoga31 @ www.portugal-a-programar.pt *
 *                                                                     *
 *  Description: Download Manager.                                     *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *)

{$mode objfpc}
unit UProgDown;

interface
uses {$ifdef unix} cthreads, cmem, {$endif}
     httpsend, synacode,
     crt, sysutils, classes, dateutils, strutils,
     UTypes, UXMLparse, UMisc, UGUIintegrator;

var PROGRAMAR : TRevistaPROGRAMAR;

function DownloadMagazine(editions : TEditionRange; path : string; const SHOWREPORT : boolean = true; const modifier : TAppModifier = ModDefault) : Int64; overload;
function DownloadMagazine(editions : TEditionRange; path : string; const modifier : TAppModifier) : Int64; overload;
function DownloadMagazine(editions : TEditionRange; path : string; out totaltime : Int64; const SHOWREPORT : boolean = true; const modifier : TAppModifier = ModDefault) : Int64;
function DownloadMagazineUI(editions : TEditionRange; path : string; out totaltime : Int64; const SHOWREPORT : boolean; out abruptend : boolean) : Int64; overload;

type TDownInfo = record
        Url, PathToSaveTo : string;
        fsize : Int64;
     end;
     PDownInfo = ^TDownInfo;

procedure DownloadFile(info : PDownInfo);


implementation

var finished : boolean;
var fs : TFileStream;

procedure DownloadFile(info : PDownInfo);
begin
   fs := TFileStream.Create(info^.PathToSaveTo, fmOpenWrite or fmCreate);
   try
      HttpGetBinary(info^.Url, fs);
   finally
      info^.fsize := fs.size;
      fs.Free;
   end;
   finished := true;
end;


function DownloadMagazine(editions : TEditionRange; path : string; const modifier : TAppModifier = ModDefault) : Int64; overload;
begin
    DownloadMagazine := DownloadMagazine(editions, path, true, modifier);
end;


function DownloadMagazine(editions : TEditionRange; path : string; const SHOWREPORT : boolean = true; const modifier : TAppModifier = ModDefault) : Int64; overload;
var tt : Int64;
begin
    DownloadMagazine := DownloadMagazine(editions, path, tt, SHOWREPORT, modifier);
end;


var uiendflag : boolean;
function DownloadMagazineUI(editions : TEditionRange; path : string; out totaltime : Int64; const SHOWREPORT : boolean; out abruptend : boolean) : Int64;
begin
    uiendflag := false;
    DownloadMagazineUI := DownloadMagazine(editions, path, totaltime, SHOWREPORT);
    abruptend := uiendflag;
end;


function DownloadMagazine(editions : TEditionRange; path : string; out totaltime : Int64; const SHOWREPORT : boolean = true; const modifier : TAppModifier = ModDefault) : Int64;
(* Downloads the given editions and save them to "path". *)
const MINSIZE = 1024;  // 1KB

var ed   : byte;
    size : Int64;
    (* Download report *)
    success : TEditionRange = [];
    invalid : TEditionRange = [];
    ignored : TEditionRange = [];
    aborted : TEditionRange = [];
    starttime : TDateTime;
    endtime   : TDateTime;
    betweentime : Int64;
    ico : TLoadIcon;
    atdown : PDownInfo;
    id : QWord;
    canceled : boolean;
    opt : char;

begin
    try
       ico := TLoadIcon.Create(false);
       try
           totaltime := 0;
           DownloadMagazine := 0;
           // Download manager:
           for ed in editions do begin
               if modifier <> ModQuiet then
                   write('Downloading ' + OrdinalNumber(ed) + ' edition (' + DateToMY(PROGRAMAR.edicoes[ed-1].info.data) + ') ');
               if not FileExists(path + 'Revista_PROGRAMAR_' + IntToStr(ed) + '.pdf') then begin
                   canceled := false;
                   finished := false;
                   New(atdown);
                   with atdown^ do begin
                      Url := PROGRAMAR.edicoes[ed-1].pdf;
                      PathToSaveTo := path + 'Revista_PROGRAMAR_' + IntToStr(ed) + '.pdf';
                      fsize := 0;
                   end;
                   opt := #0;
                   ico.SetPosition(WhereX, WhereY);
                   ico.Run := true;
                   starttime := Now;
                   BeginThread(TThreadFunc(@DownloadFile), atdown, id);
                   while (not finished) and (not canceled) do begin
                      if KeyPressed then begin
                          opt := ReadKey;
                          if opt in [#27] then begin  // More actions under investigation
                             KillThread(id);
                             canceled := true;
                             uiendflag := true;
                             DeleteFile(atdown^.PathToSaveTo);
                             break;
                          end;
                      end;
                   end;
                   if finished then begin
                      size := atdown^.fsize;
                      KillThread(id);
                   end else
                      size := 0;
                   CloseThread(id);
                   endtime := Now;
                   ico.Run := false;
                   
                   if canceled then
                      case opt of
                         #27 : begin
                                  writeln(PadLeft('[ABORTED]', SCREENWIDTH - WhereX));
                                  writeln('   * The entire operation was aborted by the user!');
                                  aborted := editions - success - ignored - invalid;
                                  break;
                               end;
                      end;
                   
                   betweentime := MilliSecondsBetween(endtime, starttime);
                   Inc(totaltime, betweentime);
                   if size < 1024 then begin  // Invalid size, delete downloaded file.
                       if (modifier <> ModQuiet) and SHOWREPORT then begin
                           writeln(PadLeft('[ERROR]', SCREENWIDTH - WhereX));
                           write('   * File size is very small (', FormatSize(size),').', CRLF, '     Deleting file... ');
                       end;
                       DeleteFile(path + 'Revista_PROGRAMAR_' + IntToStr(ed) + '.pdf');
                       writeln('[OK]');
                       Include(invalid, ed);
                       if modifier <> ModQuiet then
                           writeln(PadLeft('[INVALID] (took ' + FormatTimeToHNS(betweentime) + ')', SCREENWIDTH - WhereX));
                   end else begin  // Download completed with success.
                       Include(success, ed);
                       Inc(DownloadMagazine, size);
                       if modifier <> ModQuiet then
                           writeln(PadLeft('[OK] (' + FormatSize(size) + ' in ' + FormatTimeToHNS(betweentime) + ')', SCREENWIDTH - WhereX));
                   end;
               end else begin  // File already exists, do not change it.
                   if modifier <> ModQuiet then begin
                       writeln(PadLeft('[IGNORED]', SCREENWIDTH - WhereX));
                       writeln('   * File already exists.');
                   end;
                   Include(ignored, ed);
               end;
           end;
           
           // Download report:
           if (modifier <> ModQuiet) and SHOWREPORT then begin
               writeln('Download report:');
               writeln('   Total downloaded = ', FormatSize(DownloadMagazine), ' in ', FormatTimeToHNS(totaltime));
               write('    Invalid = ', FormatRange(invalid));
               write(CRLF, '    Ignored = ', FormatRange(ignored));
               if modifier = ModVerbose then begin
                   write(CRLF, '   Canceled = ', FormatRange(aborted));
                   write(CRLF, '    Success = ', FormatRange(success));
               end;
               writeln;
           end;
       except
           on ex:exception do
               ShowException(ex);
       end;
    finally
         ico.Destroy;
    end;
end;


initialization
    PROGRAMAR := TRevistaPROGRAMAR.Create;

finalization
    PROGRAMAR.Free;

end.
