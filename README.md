PROGRAMAR Downloader for Linux

2.1.1

Igor Nunes, aka thoga31 @ www.portugal-a-programar.pt

Licensed under the GNU GPL 3.0 (https://www.gnu.org/copyleft/gpl.html)

Language: Object Pascal (using Free Pascal Compiler, only)

This application downloads the editions of 'Revista PROGRAMAR' given by the user.

This project is on its beginning, so it's a little bit incomplete. Give your contribution! ;)

To build this application you'll need the following libraries available at http://www.ararat.cz/synapse/doku.php: blcksock, httpsend, ssfpc, synacode, synafpc, synaip, synautil and synsock.
